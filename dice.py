#!/bin/python3
# -*- coding: utf-8 -*-

import signal
import random
import os

def main():
    clear()

    signal.signal(signal.SIGTSTP, handler)

    while True:
        try:
            dice = input("[?] ")

            if dice[0:5] == "roll ":
                dice = dice.strip("roll ").split('d')
                if (int(dice[0]) <= 0 or int(dice[1]) <= 0):
                    print()
                    print("[!] Number must be more then 0!")
                    print()
                    pass
                else:
                    roll(int(dice[0]), int(dice[1]))
            elif dice == "exit":
                exit()
            elif dice == "help":
                help()
            elif dice == "clear":
                clear()
            elif dice == "about":
                about()
            else:
                print()
                print("[!] Command \"%s\" not found.\n[!] Use \"help\"" % dice)
                print()
            
        except ValueError:
            print()
            print("[!] Invalid arguments!")
            print()

        except KeyboardInterrupt:
            print()
            print("[!] Use \"exit\"")
            print()

        except EOFError:
            print()
            print("[!] Use \"exit\"")
            print()
    pass

def handler(sugnum, frame):
    print()
    print("[!] Use \"exit\"")
    print()
    pass

def roll(a, b):
    dices_sum = int()
    for i in range(1, a + 1):
        result = random.randint(1, b)
        dices_sum += result
        print("[D%i] %id%i: [%i]" % (i, a, b, result))
    print("[D] Sum: %i" % dices_sum)
    print()
    pass

def help():
    print()
    print("[H] roll {n}d{m} - roll dice (e.g. \"roll 3d6\")")
    print("[H] about - print about info")
    print("[H] clear - clear screen")
    print("[H] exit - close the program")
    print()
    pass

def clear():
    os.system("tput reset || clear || cls")
    print()
    print(" -=== Dice roller ===- \n")
    pass

def exit():
    os.sys.exit(0)

def about():
    print()
    print("[A] TechnoNest")
    print("[A] Created by oyaro")
    print()

if __name__ == "__main__":
    main()
    pass
